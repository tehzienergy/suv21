jQuery(document).ready(function($) {
  
  var sliderRows = $('.slider-rows')
  
  if (sliderRows.length) {
    sliderRows.each(function (e) {
      $(this).slick({
        infinite: true,
        rows: 2,
        autoplay: false,
        dots: true,
        arrows: true,
        slidesPerRow: 2,
        slidesToScroll: 1,
        slidesToShow: 2,
        responsive: [
          {
            breakpoint: 1200,
            settings: {
              dots: false,
              rows: 1,
              slidesToShow: 4,
              slidesPerRow: 1
            }
          },
          {
            breakpoint: 992,
            settings: {
              dots: false,
              rows: 1,
              slidesToShow: 3,
              slidesPerRow: 1
            }
          },
          {
            breakpoint: 768,
            settings: {
              dots: false,
              rows: 1,
              slidesToShow: 2,
              slidesPerRow: 1
            }
          },
          {
            breakpoint: 576,
            settings: {
              dots: false,
              rows: 1,
              slidesToShow: 1,
              slidesPerRow: 1
            }
          },
        ]
      })
    })
  }

})
