$('.presentation__content').slick({
  infinite: true,
  autoplay: false,
  dots: false,
  arrows: true,
  slidesToShow: 1,
  slidesToScroll: 1,
  prevArrow: $('.presentation__arrow--left'),
  nextArrow: $('.presentation__arrow--right')
});

var $status = $('.presentation__counter');
var $slickElement = $('.presentation__content');

$slickElement.on('init reInit afterChange', function(event, slick, currentSlide, nextSlide){
    var i = (currentSlide ? currentSlide : 0) + 1;
    $status.text(i + ' из  ' + slick.slideCount);
});
