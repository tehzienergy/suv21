jQuery(document).ready(function($) {
  
  var slider = $('.slider')
  
  if (slider.length) {
    slider.each(function (e) {
      $(this).slick({
        infinite: true,
        autoplay: false,
        dots: false,
        arrows: true,
        slidesToShow: 4,
        slidesToScroll: 1,
        responsive: [
          {
            breakpoint: 992,
            settings: {
              slidesToShow: 3
            }
          },
          {
            breakpoint: 768,
            settings: {
              slidesToShow: 2
            }
          },
          {
            breakpoint: 576,
            settings: {
              slidesToShow: 1
            }
          },
        ]
      })
    })
  }

})
