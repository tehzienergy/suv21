jQuery(document).ready(function($) {
  
  var sliderReviews = $('.slider-reviews')
  
  if (sliderReviews.length) {
    sliderReviews.each(function (e) {
      $(this).slick({
        infinite: true,
        autoplay: false,
        dots: false,
        arrows: true,
        slidesToShow: 3,
        slidesToScroll: 1,
        responsive: [
          {
            breakpoint: 992,
            settings: {
              slidesToShow: 2
            }
          },
          {
            breakpoint: 576,
            settings: {
              slidesToShow: 1
            }
          },
        ]
      })
    })
  }

})
