jQuery(document).ready(function($) {
  
  var slider = $('.speakers-slider')
  
  if (slider.length) {
    slider.each(function (e) {
      $(this).slick({
        infinite: false,
        autoplay: false,
        dots: false,
        arrows: true,
        slidesToShow: 3,
        slidesToScroll: 1,
//        centerMode: true,
        responsive: [
          {
            breakpoint: 768,
            settings: {
              slidesToShow: 1
            }
          },
          {
            breakpoint: 576,
            settings: {
              slidesToShow: 1
            }
          },
        ]
      })
    })
  }

})
